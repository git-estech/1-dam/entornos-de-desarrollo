using UnityEngine;

/*
    Hacer que las cabinas se muevan de arriba a abajo a lo largo de unos l�mites como en el
    ejercicio de transform.translate dentro de la atracci�n. (Puntua 2/10)
*/

// lo siento de antemano. espero que tengas una bolsa cerca.
public class cabinaMov : MonoBehaviour
{
    private bool isUp;

    public Vector3 moveUp   = new Vector3(0,  1, 0);
    public Vector3 moveDown = new Vector3(0, -1, 0);

    public Vector3 maxUp    = new Vector3(0,  0, 0);
    public Vector3 maxDown  = new Vector3(0, -5, 0);

    void Start() {
        isUp = true;
    }

    // Update is called once per frame
    void Update()
    {
        // no funciona, aunque es exactamente lo que yo me esperaba

        if (isUp == true) {
            transform.position = transform.position + moveDown * Time.deltaTime;
            if (transform.position == maxDown) {
                isUp = false;
            }
        }

        if (isUp == false)
        {
            transform.position = transform.position + moveUp * Time.deltaTime;
            if (transform.position == maxUp)
            {
                isUp = true;
            }
        }
    }
}
