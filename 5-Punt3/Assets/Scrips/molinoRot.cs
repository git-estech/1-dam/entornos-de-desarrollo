using UnityEngine;

/* Queremos montar una atracci�n de feria:


---------------- Parte 1 ----------------

1.  Primero creamos una base, que ser� un cilindro alargado. Este cilindro rotar� sobre si
    mismo en y. (Punt�a 2/10)

2.  Seguidamente crearemos un brazo cuyo centro se encontrar� en el extremo superior del
    cilindro y girar� 360� en el eje z (Puntua 3/10).

3.  Seguidamente, en uno de sus extremos habr� una noria con 4 cabinas alineadas a lo largo
    de un c�rculo que rotar�n alrededor de este extremo. (Puntua 3/10)


---------------- Parte 2 ----------------

    Hacer que las cabinas se muevan de arriba a abajo a lo largo de unos l�mites como en el
    ejercicio de transform.translate dentro de la atracci�n. (Puntua 2/10)

*/

public class molinoRot : MonoBehaviour
{
    public Vector3 RotationSpeed = new Vector3(0, 50, 0);

    void Update()
    {
        transform.Rotate(RotationSpeed * Time.deltaTime);
    }
}
