using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class noriaRot : MonoBehaviour
{
    public Vector3 RotationSpeed = new Vector3(0, 512, 0);

    void Update()
    {
        transform.Rotate(RotationSpeed * Time.deltaTime);
    }
}
