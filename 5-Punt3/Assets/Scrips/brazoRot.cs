using UnityEngine;

/*
    2.  Seguidamente crearemos un brazo cuyo centro se encontrar� en el extremo superior del
        cilindro y girar� 360� en el eje z (Puntua 3/10).
*/

public class brazoRot : MonoBehaviour
{
    public Vector3 RotationSpeed = new Vector3(0, 0, 10);

    void Update()
    {
        transform.Rotate(RotationSpeed * Time.deltaTime);
    }
}
