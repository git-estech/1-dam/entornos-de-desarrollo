using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*

---------------- Tarea 1 ----------------

Vamos a mover un objeto en de manera uniforme a cualquier direcci�n XY. 
Cuando movamos el objeto en X este rotar� en eje Y a 120 grados. La direcci�n de rotaci�n
variar� en funci�n de la direcci�n a la que vayamos. Si vamos a la izquierda, la rotaci�n
ser� positiva y si vamos a la derecha, ser� negativa.

De igual manera, si nos movemos en Y, la rotaci�n ser� en X en 50 grados. Si vamos hacia
arriba la rotaci�n ser� en X positivo y hacia abajo en X negativo.


---------------- Tarea 2 ----------------

Mientras pulsemos el dedo moviendonos en una direcci�n, la velocidad no parar� de aumentar.
En cuanto soltemos dicho bot�n, la velocidad volver� a ser la inicial.

*/

public class movPlayer : MonoBehaviour
{
    public float Speed = 10;

    /* no s� si esto es lo correcto */
    public Vector3 RotationSpeedX = new Vector3(0, 120, 0);
    public Vector3 RotationSpeedY = new Vector3(50, 0, 0);

    Vector3 playerPos;

    void Update()
    {

        float h = Input.GetAxis("Horizontal");
        float v = Input.GetAxis("Vertical");


        playerPos = Vector3.right * h + Vector3.up * v;
        playerPos.Normalize();
        transform.position += playerPos * Time.deltaTime * Speed;


        /* https://docs.unity3d.com/ScriptReference/Transform.Rotate.html */

        if (h > 0) {
            transform.Rotate(RotationSpeedX * Time.deltaTime);
        }
        else if (h < 0) {
            transform.Rotate(-RotationSpeedX * Time.deltaTime);
        }

        if (v > 0) {
            transform.Rotate(RotationSpeedY * Time.deltaTime);
        }
        else if (v < 0) {
            transform.Rotate(-RotationSpeedY * Time.deltaTime);
        }

    }
}
