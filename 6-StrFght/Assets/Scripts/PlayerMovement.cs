using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour {

    [Range(0,100)]
    public float MaxSpeed, MaxAcceleration;
    [Header("Bounds")]
    public float HorizontalLimits;
    public float VerticalLimits;

    private float currentVelocityX, currentVelocityY;

    void Update() {


        // Input
        float h = Input.GetAxisRaw("Horizontal");
        float v = Input.GetAxisRaw("Vertical");


        // Dirección hacia la que nos queremos mover
        Vector3 dir = new Vector3(h, v).normalized;


        // Aceleración (cambio en la velocidad) por cada dimensión
        currentVelocityX = Mathf.MoveTowards(currentVelocityX, dir.x * MaxSpeed, MaxAcceleration * Time.deltaTime);
        currentVelocityY = Mathf.MoveTowards(currentVelocityY, dir.y * MaxSpeed, MaxAcceleration * Time.deltaTime);


        // Velocity es el vector que indica la velocidad por segundo...
        Vector3 velocity = new Vector3(currentVelocityX, currentVelocityY);
        // ... que hay que pasarlo a movimiento por cada instante, eso es el desplazamiento
        Vector3 displacement = velocity * Time.deltaTime;

        transform.Translate(displacement);


        // Limites de movimiento a los bordes de la pantalla
        float xClamped = Mathf.Clamp(transform.position.x, -HorizontalLimits, HorizontalLimits);
        float yClamped = Mathf.Clamp(transform.position.y, -VerticalLimits,   VerticalLimits);

        transform.position = new Vector3(xClamped, yClamped);
    }
}
