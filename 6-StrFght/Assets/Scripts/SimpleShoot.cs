using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimpleShoot : MonoBehaviour
{
    public GameObject bullet;
    public float      Cooldown;
    public Transform  SpawnPoint;

    private float     cdCounter = 0;


    void Start() {
        cdCounter = Cooldown; // No tener cooldown nada más empezar el juego
    }


    void Update() {
        cdCounter += Time.deltaTime;

        if (Input.GetKey(KeyCode.Space) && cdCounter > Cooldown) {
            // Instanciar (crear) la bala
            Instantiate(bullet, SpawnPoint.position, Quaternion.identity);
            cdCounter = 0;

        }
    }
}
