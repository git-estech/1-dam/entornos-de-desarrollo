using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIPlayerHP : MonoBehaviour
{
    public Slider       HealthBar;
    public HealthSystem HealthSystem;
    
    void Start()
    {
        HealthBar.maxValue = HealthSystem.maxHP;
    }
    
    void Update()
    {
        HealthBar.value = HealthSystem.currentHP;
    }
}
