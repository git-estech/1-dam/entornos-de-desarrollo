using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthSystem : MonoBehaviour 
{
    public int maxHP;

    // He creado esta variable para poner el daño que da cada gameobject
    // en el inspector en vez de duplicar código
    public int damage;
    
    [HideInInspector]
    public int currentHP;

    public string DamageBulletTag;
    public string HealthTag;

    public string MineTag;
    public GameObject explosion;
    public Transform SpawnPoint;


    void Start() {
        currentHP = maxHP;
    
    }

/*    void Update() {
        Debug.Log(currentHP);
    }*/

    private void OnTriggerEnter2D(Collider2D collision) {

        // Daño
        if (collision.CompareTag(DamageBulletTag)) {
            Debug.Log("ha entrado en contacto");
            currentHP -= damage;

            if (currentHP <= 0) Destroy(gameObject);
        }

        // Explosion (animacion mina)
        if (collision.CompareTag("Bullet")) {
            Instantiate(explosion, SpawnPoint.position, Quaternion.identity);
            Destroy(gameObject);
        }

        // Salud
        if (collision.CompareTag(HealthTag)) {
            Debug.Log("salud: ha entrado en contacto");

            if (currentHP < maxHP) currentHP++;
        }
    }

}
