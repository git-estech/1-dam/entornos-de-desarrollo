using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BGscroll : MonoBehaviour {

    public  SpriteRenderer graphic;
    public  float Speed = 10; // Poner velocidad inicial
    private float gfxSize;
    private float initialYPos;
    private float yLimit;


    void Start() {

        // Guardamos lo largo que mide el grafico y lo multiplicamos por su escala
        gfxSize     = graphic.bounds.size.y;

        // Guardamos la posicion inicial...
        initialYPos = transform.position.y;

        // ...y guardamos la cantidad de distancia que debe recorrer el grafico antes de volver a su posicion inicial
        yLimit      = initialYPos - gfxSize;
    }


    void Update() {

        // Movemos el grafico
        transform.Translate(Vector3.up * Time.deltaTime * -Speed);

        // Comprobamos que el grafico no haya alcanzado el limite inferior...
        if (transform.position.y < yLimit) {

            // ...y lo volvemos a llevar a donde estaba inicialmente
            transform.position = new Vector3(transform.position.x, initialYPos, transform.position.z);
        }
    }
}
