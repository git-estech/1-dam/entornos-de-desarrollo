using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VerticalMovement : MonoBehaviour
{
    // Vertical movement script for the bullet

    // public float x, y;
    public float Speed = 10;
    public float delayToDestroy;
    //Vector3 pos;


    void Update()
    {
        /*
        pos = new Vector3(x, y);
        transform.position += pos * Time.deltaTime;
        */

        transform.Translate(Speed * Time.deltaTime * Vector3.up);

        Destroy(this.gameObject, delayToDestroy);
    }
}
