/*

----------------------------------------------- Actividad 2 -----------------------------------------------
hacer lo mismo que en el caso anterior, en una escena nueva, solo que esta vez los tramos de doble de
velocidad ser�n aquellos donde la parte no decimal de x sea par.

Ejemplo:
    Si el objeto se encuentra en la posici�n x--> 2.34, ir� al doble de velocidad, si se encuentra en 3.9
    ir� a velocidad normal, en 4.01 ir� al doble de velocidad y as�. 

La funci�n Mathf.RoundToInt os puede ayudar aqu�. (gracias)
 
*/


using UnityEngine;

public class movimiento2 : MonoBehaviour
{
    Vector3 playerPosition;
    public float Speed;
    public int PlayerPositionX;

    void Update()
    {
        float h = Input.GetAxis("Horizontal");
        float v = Input.GetAxis("Vertical");

        playerPosition = Vector3.right * h + Vector3.up * v;
        playerPosition.Normalize();
        transform.position += playerPosition * Time.deltaTime * Speed;

        PlayerPositionX = Mathf.RoundToInt(transform.position.x);

        if (PlayerPositionX % 2 == 0) {
            Speed = 20;
            Debug.Log(PlayerPositionX);
        }
        else {
            Speed = 10;
        }
    }
}
