/*

----------------------------------------------- Actividad 1 -----------------------------------------------
Hacer que un cubo se mueva en todas las direcciones a la misma velocidad, no obstante la velocidad cambiar�
seg�n donde est�. La velocidad debe poder ser insertada por inspector.

Si el cubo se encuentra en una posici�n x > 10, x < 20 , y < 10 entonces el cubo duplicar� su velocidad.
 
*/


using UnityEngine;

public class movimiento : MonoBehaviour
{
    Vector3 playerPosition;
    public float Speed;

    void Update()
    {
        float h = Input.GetAxis("Horizontal");
        float v = Input.GetAxis("Vertical");

        playerPosition = Vector3.right * h + Vector3.up * v;
        playerPosition.Normalize();
        transform.position += playerPosition * Time.deltaTime * Speed;

        if ((transform.position.x > 10) && (transform.position.x < 20 ) && (transform.position.y < 10)) {
            Speed = 20;
            Debug.Log(transform.position);
        } else {
            Speed = 10;
        }
    }
}
