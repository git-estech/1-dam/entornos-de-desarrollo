using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement2 : MonoBehaviour
{
    public float Speed = 4;
    public Space relativeTo;
    public float SpeedX = 5;
    public float SpeedY = 3;

    public float RotSpeed = 120;

    //  Start is called before the first frame update
    void Start()
    {

    }

//  Update is called once per frame
    void Update()
    {

    //  Capturar input usuario
        float h = Input.GetAxis("Horizontal");
        float v = Input.GetAxis("Vertical");
        // Debug.Log(z);

    //  Asignamos ejes en el que queremos movernos
        Vector3 newHPos = Vector3.right * h * SpeedX;
        Vector3 newVPos = Vector3.up * v * SpeedY;
        
    //  Direcci�n (vector tama�o 1)
        Vector3 dir = (newHPos + newVPos);

    //  Cambio de posici�n por segundo (pero en Unity nos movemos cada fotograma)
        Vector3 velocity = dir * Speed;

    //  Cambio de posici�n por fotograma
        Vector3 displacement = velocity * Time.deltaTime;

    //  Movimiento acorde a un espacio de coordenadas
        transform.Translate(displacement, relativeTo);


    //  Rotando el cubo
        Vector3 cubeRot = Vector3.left * RotSpeed * Time.deltaTime;
        transform.Rotate(cubeRot);
    }
}
