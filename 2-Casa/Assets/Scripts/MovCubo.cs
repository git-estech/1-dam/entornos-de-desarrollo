using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class MovCubo : MonoBehaviour
{
    public TextMeshProUGUI Text;
    public float Speed = 5;

    void Start()
    {

    }

    void Update()
    {
        float h = Input.GetAxis("Horizontal");
        float v = Input.GetAxis("Vertical");

        if (h < 0)
        {
            Vector3 displacement = (Vector3.right * h + Vector3.up * v);
            displacement.Normalize();

            transform.position = displacement * 3;
        }
        else if (v < 0)
        {
            Vector3 displacement = (Vector3.right * h + Vector3.up * v);
            displacement.Normalize();

            transform.position = displacement * 3;
        }
               
        // text.text = transform.position.ToString();

    }
}
