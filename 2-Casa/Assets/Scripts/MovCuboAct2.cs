using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovCuboAct2 : MonoBehaviour
{
    public float Speed = 5;

    void Start()
    {
        
    }

    void Update()
    {
        Vector3 upMov = Vector3.up * Speed * Time.deltaTime * 2;
        Vector3 rightMov = Vector3.right * Speed * Time.deltaTime * 2;
        Vector3 downMov = Vector3.down * Speed * Time.deltaTime * 2;

        if (transform.position.y <= 3.5)
        {
            transform.Translate(upMov, Space.Self);
            transform.Translate(rightMov / 2, Space.Self);
        }
        else if (transform.position.y >= 3.5)
        {
            transform.Translate(rightMov, Space.Self);
            transform.Translate(downMov, Space.Self);
        }
    }
}
