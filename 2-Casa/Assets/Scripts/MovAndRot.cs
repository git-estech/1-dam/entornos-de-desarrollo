using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovAndRot : MonoBehaviour
{
    public float Speed = 5;
    public float RotSpeed = 120;
    // public Space relativeTo;

    void Update()
    {
    //  Capturar input usuario
        float h = Input.GetAxis("Horizontal");
        float v = Input.GetAxis("Vertical");

    //  Asignamos ejes en el que queremos movernos
        Vector3 forwardMov = Vector3.forward * v * Speed * Time.deltaTime;
        transform.Translate(forwardMov, Space.Self);



        Vector3 angularMov = Vector3.up * RotSpeed * Time.deltaTime * h;
        transform.Rotate(angularMov);
    }
}
