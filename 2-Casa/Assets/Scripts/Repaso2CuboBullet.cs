using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Repaso2CuboBullet : MonoBehaviour
{
    public float Speed;
    public float TiempoVida;
    public Space RelativeTo;

    // Start is called before the first frame update
    void Start()
    {
        /* Destruir(este.GameObject, tras pasar TiempoVida) */
        Destroy(this.gameObject, TiempoVida);
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(Speed * Vector3.right * Time.deltaTime, RelativeTo);
    }
}
