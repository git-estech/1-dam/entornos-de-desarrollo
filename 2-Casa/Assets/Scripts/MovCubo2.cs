using System.Collections;
using System.Collections.Generic;
using UnityEngine;


// 1� Hacer que un objeto rote alrededor de un eje central simulando una �rbita.
// 2� Hacer que el objeto rotatorio pueda moverse en el plano XY pero que no pare de girar en su misma �rbita.
// 3� Hacer que el propio objeto gire en eje X sobre si mismo.


public class MovCubo2 : MonoBehaviour
{
    public float RotSpeed = 120;

    void Start()
    {

    }

    void Update()
    {
        Vector3 angularMov = Vector3.forward * RotSpeed * Time.deltaTime;
        transform.Rotate(angularMov);
    }
}
