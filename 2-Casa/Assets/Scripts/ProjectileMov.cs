using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileMov : MonoBehaviour {
    public float Speed;
    public Space RelativeTo;
    public float Lifespan;

    // Start is called before the first frame update
    void Start() {
        // borrar la instancia de go a los dos segundos
        Destroy(this.gameObject, Lifespan);
    }

    // Update is called once per frame
    void Update() {
        transform.Translate(Speed * Vector3.up * Time.deltaTime, RelativeTo);
    }
}
