using UnityEngine;

public class repaso : MonoBehaviour {


    //  Posicion GameObject
    Vector3       direction;
    public  float limitePositionX;
    public  float limitePositionY;


    //  Velocidad GameObject
    public  float Speed;
    private float currentVelocityX;
    private float currentVelocityY;

    [Range(0, 100)] // Este rango solo se aplica a la variable declarada justo debajo
    public  float maxSpeed, maxAcceleration;


    //  Bullets
    public  GameObject bullet;
    public  Transform SpawnPoint1, SpawnPoint2;

    public  float CoolDown;
    public  float currentCoolDown { get; private set; }


    private void Update() {

        float HorizontalPos = Input.GetAxisRaw("Horizontal");
        float VerticalPos   = Input.GetAxisRaw("Vertical");


        //  Posicion
        direction = Vector3.right * HorizontalPos + Vector3.up * VerticalPos;
        direction.Normalize();


        //  Aceleracion
        currentVelocityX = Mathf.MoveTowards(currentVelocityX, direction.x * maxSpeed, maxAcceleration * Time.deltaTime);
        currentVelocityY = Mathf.MoveTowards(currentVelocityY, direction.y * maxSpeed, maxAcceleration * Time.deltaTime);
        transform.position += new Vector3(currentVelocityX, currentVelocityY) * Time.deltaTime;


        //  Cooldown y bullet
        currentCoolDown += Time.deltaTime;

        if (Input.GetKey(KeyCode.Space) && currentCoolDown >= CoolDown) {

            //  Instancia (bullet, posicion del objeto que llama al script, rotacion del componente)
            Instantiate(bullet, SpawnPoint1.position, bullet.transform.rotation);
            Instantiate(bullet, SpawnPoint2.position, bullet.transform.rotation);

            //  Cooldown
            currentCoolDown = 0;

            //  El lifetime del bullet esta dentro de ProjectileMov, ya que este script es exclusivamente para la nave.

        }


        //  Limitar rango de posicion X e Y del GameObject para que no salga de la pantalla
        float limitePositionX = Mathf.Clamp(transform.position.x, -45, 45);
        float limitePositionY = Mathf.Clamp(transform.position.y, 0, 40);

        transform.position = new Vector3(limitePositionX, limitePositionY);


    }
}
