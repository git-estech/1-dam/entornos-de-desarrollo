using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

// Este c�digo realiza una traslaci�n desde el centro de coordenadas (0, 0, 0) a
// una direcci�n con una distancia determinada que siempre es la misma.

// Hemos hecho una modificaci�n con el if que hace que solo pueda hacer esta traslaci�n
// en valores positivos en x limitando el movimiento en dicho eje.

public class FixedDisplacement : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        // Capturamos el input del usuario
        float h = Input.GetAxis("Horizontal");
        float v = Input.GetAxis("Vertical");

        // Filtramos el input de forma que si pulsamos izq. se queda en 0
        if (h < 0)
        {
            h = 0;
        }

        Vector3 displacement = (Vector3.right * h + Vector3.up * v);
        displacement.Normalize();
        transform.position = displacement * 3;
    }
}
